# Fonte: https://www.tutorialspoint.com/python/

import tkinter as tk
from drive.drive import GoogleDrive


class Interface(tk.Frame):
    def __init__(self, master=None):
        super().__init__(master)
        self.master = master
        self.pack()
        self.create_widgets()

    def create_widgets(self):
        btn_open = tk.Button(self.master)
        btn_open["text"] = "Abrir dados"
        btn_open["command"] = self.open
        btn_open.pack(side="left")

        frame = tk.Frame(self.master, width=10, height=10)
        frame.pack(side=tk.RIGHT)

        self.listbox = tk.Listbox(frame)

        scrollbarx = tk.Scrollbar(frame, orient=tk.HORIZONTAL, command=self.listbox.xview)
        scrollbarx.pack(side=tk.BOTTOM, fill=tk.X)
        scrollbary = tk.Scrollbar(frame, orient=tk.VERTICAL, command=self.listbox.yview)
        scrollbary.pack(side=tk.RIGHT, fill=tk.Y)

        self.listbox['xscrollcommand'] = scrollbarx.set
        self.listbox['yscrollcommand'] = scrollbary.set
        self.listbox.pack(side=tk.TOP)

        btn_quit = tk.Button(self.master, text="Encerrar", fg="red", command=self.master.destroy)
        btn_quit.pack(side="bottom")

    def openList(self, dataframe):

        # fonte da lógica: https://www.javatpoint.com/python-tkinter-scrollbar

        for row in dataframe.iteritems():
            # print(row[1])
            self.listbox.insert(tk.END, row[1])


    def open(self):
        dataframe = GoogleDrive().openSheet()['NOME: ']
        self.openList(dataframe.T)