import gspread
from oauth2client.service_account import ServiceAccountCredentials
from pandas import DataFrame

class GoogleDrive():
    def __init__(self):
        scopes = ['https://www.googleapis.com/auth/spreadsheets', 'https://www.googleapis.com/auth/drive']
        # o link abaixo é gerado em um projeto do google cloud para fornecer acessos as APIs do google
        credentials = ServiceAccountCredentials.from_json_keyfile_name('D:\Cursos\pessoal\secrets\client_secrets.json', scopes)
        self.gautho = gspread.authorize(credentials)

    def openSheet(self, link='https://docs.google.com/spreadsheets/d/1A8E1ymq8dRDh6t8mUvPX4dVMm-cg2QGW97XKAexjNOQ/edit#gid=1643697732', ws='Respostas ao formulário 1'):
        # Abre a planilha do google drive
        sheet = self.gautho.open_by_url(link)
        # Acessa a aba específica da planilha
        worksheet = sheet.worksheet(ws)
        # Retorna um datafame pandas para análise dos dados
        return DataFrame(worksheet.get_all_records())
