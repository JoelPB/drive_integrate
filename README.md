# Projeto em Python para acessar um planilha do GoogleDrive e tratar os dados estatísticamente

##### Para isso foram utilizados principalmente a bibliotecas:
###### gspread para acessar a planilha: https://docs.gspread.org/en/latest/oauth2.html#enable-api-access-for-a-project
###### pandas para fazer o tratamento estatístico: https://pandas.pydata.org/docs/user_guide/index.html
